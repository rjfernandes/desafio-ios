//
//  PullRequestCell.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit
import SwiftDate
import FontAwesome_swift

class PullRequestCell: BaseGithubCell {
    
    @IBOutlet weak var lblPullRequestDate: UILabel!
    @IBOutlet weak var iconPullRequestState: UILabel!
    
    var pullRequest: PullRequestModel! {
        didSet {
            populate()
            user = pullRequest.user
        }
    }
    
    func populate() {
        lblTitle.text = pullRequest.title
        lblDescription.text = pullRequest.body
        imgAvatar.round().load(url: pullRequest.user?.avatar, default: #imageLiteral(resourceName: "ic_profile_default"))
        lblUsername.text = pullRequest.user?.username
        lblFullName.text = pullRequest.user?.name
        
        iconPullRequestState.isHidden = !pullRequest.isOpen
        if pullRequest.isOpen {
            iconPullRequestState.font = UIFont.fontAwesome(ofSize: 18)
            iconPullRequestState.text = String.fontAwesomeIcon(name: .folderOpenO)
        }
        
        if let fmtDate = pullRequest.createDate?.string(format: .custom(NSLocalizedString("dateMask", comment: ""))) {
            lblPullRequestDate.text = "\(NSLocalizedString("Created at", comment: "")) \(fmtDate)"
        }
        else {
            lblPullRequestDate.text = ""
        }
    }
}

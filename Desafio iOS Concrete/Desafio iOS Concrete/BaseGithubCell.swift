//
//  BaseGithubCell.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit

class BaseGithubCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblFullName: UILabel!
    
    var user: UserModel? {
        didSet {
            populateUser()
        }
    }
    
    lazy var service = GithubService()

    func populateUser() {
        guard let user = user, user.name == nil else { return }
        self.lblFullName.text = "..."
        service.requestName(user: user) { success in
            if success {
                self.lblFullName.text = user.name
            }
            else {
                self.lblFullName.text = ""
            }
        }
    }
}

//
//  UserModel.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import Foundation
import ObjectMapper

class UserModel: Mappable {
    
    var name:String?
    var username = ""
    var profileURL = ""
    var avatar: String?

    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name        <- map["name"]
        username    <- map["login"]
        avatar      <- map["avatar_url"]
        profileURL  <- map["url"]
    }
}

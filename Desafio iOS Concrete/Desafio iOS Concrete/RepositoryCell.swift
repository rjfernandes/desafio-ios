//
//  RepositoryCell.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit
import FontAwesome_swift

class RepositoryCell: BaseGithubCell {
    
    @IBOutlet weak var lblIconForks: UILabel!
    @IBOutlet weak var lblForks: UILabel!
    @IBOutlet weak var lblIconStars: UILabel!
    @IBOutlet weak var lblStars: UILabel!
    
    var repository: RepositoryModel! {
        didSet {
            populate()
            user = repository.owner
        }
    }
    
    private func populate() {
        lblTitle.text = repository.name
        lblDescription.text = repository.descript
        
        let font = UIFont.fontAwesome(ofSize: 18)
        
        lblIconForks.font = font
        lblIconForks.text = String.fontAwesomeIcon(name: .codeFork)
        lblForks.text = "\(repository.forks)"

        lblIconStars.font = font
        lblIconStars.text = String.fontAwesomeIcon(name: .starO)
        lblStars.text = "\(repository.stars)"
        
        imgAvatar.round().load(url: repository.owner?.avatar, default: #imageLiteral(resourceName: "ic_profile_default"))
        lblUsername.text = repository.owner?.username
        lblFullName.text = repository.owner?.name
    }
}

//
//  PullRequestModel.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import ObjectMapper
import SwiftDate

class PullRequestModel: Mappable {
    var title = ""
    var body = ""
    var urlPage = ""
    var createDate: Date?
    var user: UserModel?
    var isOpen = true
    
    var orderNo: Int {
        return isOpen ? 0 : 1
    }

    required init?(map: Map) { }
    
    func mapping(map: Map) {
        let transformDate = TransformOf<Date, String>(fromJSON: { value in
            if let value = value {
                return self.dateFormatter().date(from: value)
            }
            return nil
        }) { date in
            if let date = date {
                return self.dateFormatter().string(from: date)
            }
            return nil
        }
        
        let transformOpen = TransformOf<Bool, String>(fromJSON: {
            return ($0 ?? "closed" == "open")
        }) {
            return $0 ?? false ? "closed" : "open"
        }
        
        title       <- map["title"]
        body        <- map["body"]
        urlPage     <- map["html_url"]
        createDate  <- (map["created_at"], transformDate)
        user        <- map["user"]
        isOpen      <- (map["state"], transformOpen)
    }
    
    private func dateFormatter() -> DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        df.timeZone = TimeZone(identifier: "America/Sao_Paulo")
        return df
    }
}

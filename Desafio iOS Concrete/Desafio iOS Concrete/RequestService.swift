//
//  RequestService.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RequestService {
    
    private var _isRequesting = false
    
    var isRequesting: Bool {
        return _isRequesting
    }
    
    private func createRequest(url: String, isGet: Bool? = nil, parameters: [String: Any]? = nil) -> DataRequest {
        print(url)
        return Alamofire.request(url, method: isGet ?? (parameters == nil) ? .get : .post, parameters: parameters)
    }
    
    private func manageResponseError(data: Data?, response: HTTPURLResponse?) -> ConcreteError? {
        let statusCode = response?.statusCode ?? 200
        guard let data = data, statusCode != 200 else {
            return nil
        }
        
        if let dictionaryError = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [String: Any], let message = dictionaryError?["message"] as? String {
            return ConcreteError.fail(descript: message)
        }
        return nil
    }
    
    func request<T:Mappable>(url: String, isGet: Bool? = nil, parameters: [String: Any]? = nil, completion: @escaping (T?, ConcreteError?) -> Void) {
        _isRequesting = true
        createRequest(url: url, isGet: isGet, parameters: parameters)
            .responseObject { (response: DataResponse<T>) in
                self._isRequesting = false
                if let responseError = self.manageResponseError(data: response.data, response: response.response) {
                    completion(nil, responseError)
                }
                else {
                    switch response.result {
                        case .success(let value):
                            completion(value, nil)
                        case .failure(let error):
                            completion(nil, ConcreteError.fail(descript: error.localizedDescription))
                    }
                }
            }
    }
    
    func request<T:Mappable>(url: String, isGet: Bool? = nil, parameters: [String: Any]? = nil, completion: @escaping ([T], ConcreteError?) -> Void) {
        _isRequesting = true
        createRequest(url: url, isGet: isGet, parameters: parameters)
            .responseArray { (response: DataResponse<[T]>) in
                self._isRequesting = false
                if let responseError = self.manageResponseError(data: response.data, response: response.response) {
                    completion([], responseError)
                }
                else {
                    switch response.result {
                        case .success(let value):
                            completion(value, nil)
                        case .failure(let error):
                            completion([], ConcreteError.fail(descript: error.localizedDescription))
                    }
                }
            }
    }
    
    func requestDictionary(url: String, isGet: Bool? = nil, parameters: [String: Any]? = nil, completion: @escaping ([String: Any]?, Error?) -> Void) {
        
        _isRequesting = true
        createRequest(url: url, isGet: isGet, parameters: parameters)
            .responseJSON { response in
                self._isRequesting = false
                switch response.result {
                    case .success(let value):
                        completion(value as? [String: Any], nil)
                    case .failure(let error):
                        completion(nil, error)
                }
            }
    }
}

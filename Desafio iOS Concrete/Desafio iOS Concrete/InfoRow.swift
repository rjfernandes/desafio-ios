//
//  InfoRow.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 14/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import Foundation

struct InfoRow {
    var error: ConcreteError?

    var isLoading: Bool {
        return error == nil
    }
    
    init() {}
    
    init(error: ConcreteError?) {
        self.error = error
    }
    
    var identifier: String {
        return isLoading ? "cellLoading" : "cellError"
    }
}

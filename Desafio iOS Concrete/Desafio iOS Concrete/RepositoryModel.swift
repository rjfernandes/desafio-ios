//
//  RepositoryModel.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import Foundation
import ObjectMapper

class RepositoryModel: Mappable {
    
    var name = ""
    var descript = ""
    var forks = 0
    var stars = 0
    var owner: UserModel?
    private var pulls_url = ""
    
    var pullRequestURL: String {
        return "\(pulls_url.replacingOccurrences(of: "{/number}", with: ""))?state=all"
    }
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name        <- map["name"]
        descript    <- map["description"]
        forks       <- map["forks"]
        stars       <- map["stargazers_count"]
        pulls_url   <- map["pulls_url"]
        owner       <- map["owner"]
    }
}

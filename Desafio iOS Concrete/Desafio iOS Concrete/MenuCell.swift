//
//  MenuCell.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 14/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgSelected: UIImageView!
    
    var item: LanguageItem! {
        didSet {
            lblTitle.text = item.title
            imgSelected.image = imgSelected.image?.withRenderingMode(.alwaysTemplate)
            imgSelected.tintColor = UIColor(red: 82.0/255.0, green: 63.0/255.0, blue: 127.0/255.0, alpha: 1)
        }
    }
    
}

//
//  MenuTableViewController.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit
import Material

class MenuTableViewController: BaseTableViewController {
    
    var pickedItem: LanguageItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickedItem = LanguageItem(title: "Java", language: "Java")
        items += [
            pickedItem,
            LanguageItem(title: "Swift", language: "Swift"),
            LanguageItem(title: "Kotlin", language: "kotlin"),
            LanguageItem(title: "PHP", language: "php"),
            LanguageItem(title: "Python", language: "python")
        ]
        tableView.reloadData()
    }
    
    override func customCell(cell: UITableViewCell, indexPath: IndexPath, object: Any) {
        if let item = object as? LanguageItem, let cell = cell as? MenuCell {
            cell.item = item
            cell.imgSelected.isHidden = item.language != pickedItem.language
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard
            let item = items[indexPath.row] as? LanguageItem,
            let repositoryController = appDelegate?.repositoryController
        else {
            return
        }
        
        pickedItem = item
        tableView.reloadData()
        
        (appDelegate?.drawerOrSplitController as? NavigationDrawerController)?.closeLeftView()
        repositoryController.newRequest(languagemItem: item)
    }
}

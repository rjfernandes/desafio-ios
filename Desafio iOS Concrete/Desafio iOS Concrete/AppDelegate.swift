//
//  AppDelegate.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit
import Material

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    /// Property to drawer (iPhone) or split controller (iPad)
    var drawerOrSplitController: UIViewController?
    
    var menuController: MenuTableViewController? {
        if let drawerController = drawerOrSplitController as? NavigationDrawerController {
            return drawerController.leftViewController as? MenuTableViewController
        }

        return (drawerOrSplitController as? UISplitViewController)?.viewControllers.first as? MenuTableViewController
    }
    
    var repositoryController: RepositoryTableViewController? {
        var navigationController: UINavigationController? = nil
        if let drawerController = drawerOrSplitController as? NavigationDrawerController {
            navigationController = drawerController.rootViewController as? UINavigationController
        }
        else if let splitController = drawerOrSplitController as? UISplitViewController, let navController = splitController.viewControllers.last as? UINavigationController {
            if navController.viewControllers.count > 1 {
                navController.popViewController(animated: true)
                return navController.viewControllers.first as? RepositoryTableViewController
            }

            navigationController = navController
        }
        return navigationController?.viewControllers.last as? RepositoryTableViewController
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        createLayout()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = drawerOrSplitController
        window?.makeKeyAndVisible()
        
        return true
    }
    
    private func createLayout() {
        let sbDrawer = UIStoryboard(name: "Drawer", bundle: nil)
        
        guard
            let mainController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        else {
            return
        }

        if let menuController = sbDrawer.instantiateInitialViewController(), UI_USER_INTERFACE_IDIOM() == .pad {
            let splitController = UISplitViewController()
            splitController.viewControllers = [ menuController, mainController ]
            splitController.preferredDisplayMode = .allVisible
            drawerOrSplitController = splitController
        }
        else {
            let menuController = sbDrawer.instantiateViewController(withIdentifier: "MenuTableViewController")
            drawerOrSplitController = NavigationDrawerController(rootViewController: mainController, leftViewController: menuController)
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
}


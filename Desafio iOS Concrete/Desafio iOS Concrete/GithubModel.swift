//
//  GithubModel.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import Foundation
import ObjectMapper

class GithubModel: Mappable {
    var total = 0
    var items = [RepositoryModel]()
        
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        total   <- map["total_count"]
        items   <- map["items"]
    }
}

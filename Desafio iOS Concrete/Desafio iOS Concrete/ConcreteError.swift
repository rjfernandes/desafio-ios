//
//  ConcreteError.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 14/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import Foundation

enum ConcreteError: Error {
    case fail(descript: String)
    
    var localizedDescription: String {
        switch self {
            case .fail(let descript):
                if descript.contains("API rate limit exceeded") {
                    return "Request API rate limit exceeded. Try again later."
                }
                return descript
        }
    }
}

//
//  ErrorCell.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 14/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit

class ErrorCell: UITableViewCell {
    @IBOutlet weak var imgError: UIImageView!
    @IBOutlet weak var lblError: UILabel!
    
    var error: ConcreteError! {
        didSet {
            imgError.image = imgError.image?.withRenderingMode(.alwaysTemplate)
            imgError.tintColor = .red
            lblError.text = NSLocalizedString(error.localizedDescription, comment: "")
        }
    }
}

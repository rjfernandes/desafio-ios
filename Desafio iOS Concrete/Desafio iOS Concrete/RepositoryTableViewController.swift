//
//  RepositoryTableViewController.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit
import Material

class RepositoryTableViewController: BaseTableViewController {

    lazy var service = GithubService()
    lazy var loadingErrorRow = InfoRow()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addMenu()
        newRequest(languagemItem: LanguageItem(title: "Java", language: "Java"))
    }
    
    func addMenu() {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            return
        }
        let backButton = UIBarButtonItem(image: Icon.menu, style: .plain, target: self, action: #selector(openMenu))
        navigationItem.leftBarButtonItem = backButton
    }
    
    func openMenu() {
        (appDelegate?.drawerOrSplitController as? NavigationDrawerController)?.openLeftView()
    }
    
    func newRequest(languagemItem item: LanguageItem) {
        self.title = "Github \(item.title)Pop"
        service.changeLanguage(languageItem: item)

        items.removeAll()
        items += [ loadingErrorRow ]
        populate()
        request()
    }
    
    func request() {
        if service.isRequesting {
            return
        }
        
        if loadingErrorRow.error != nil {
            loadingErrorRow.error = nil
            tableView.reloadData()
        }
        
        service.repositories { hasMoreRecords, github, error in
            if let error = error {
                self.loadingErrorRow.error = error
                self.tableView.reloadData()
            }
            else if let github = github {
                self.populate(elements: github.items, hasMoreRecords: hasMoreRecords)
            }
        }
    }
    
    func populate(elements: [RepositoryModel] = [], hasMoreRecords: Bool = true) {
    
        let insertIndex = items.count - 1
        items.insert(contentsOf: elements as [Any], at: insertIndex)

        if !hasMoreRecords {
           items.removeLast()
        }
        
        tableView.reloadData()
    }

    override func customCell(cell: UITableViewCell, indexPath: IndexPath, object: Any) {
        if let cell = cell as? RepositoryCell, let item = object as? RepositoryModel {
            cell.repository = item
        }
        else if let infoRow = object as? InfoRow {
            if let aciLoading = cell.viewWithTag(100) as? UIActivityIndicatorView {
                aciLoading.startAnimating()
                request()
            }
            else if let cell = cell as? ErrorCell, let error = infoRow.error {
                cell.error = error
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let item = items[indexPath.row] as? InfoRow, item.error != nil {
            request()
        }
        else if let item = items[indexPath.row] as? RepositoryModel {
            performSegue(withIdentifier: "Repositories_RepositorySpecific", sender: item)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PullRequestTableViewController, let repository = sender as? RepositoryModel {
            vc.repository = repository
        }
    }
}


//
//  BaseTableViewController.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit
import Material

class BaseTableViewController: UITableViewController {
    
    var items = [Any]()
    
    var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        autoHeight()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if navigationController?.viewControllers.count ?? 0 > 1 {
            let backButton = UIBarButtonItem(image: Icon.arrowBack, style: .plain, target: self, action: #selector(actBack))
            navigationItem.leftBarButtonItem = backButton
        }
    }
    
    func actBack() {
        navigationController?.popViewController(animated: true)
    }
    
    func autoHeight() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    func identifier(indexPath: IndexPath) -> String {
        let item = items[indexPath.row]
        return (item as? InfoRow)?.identifier ?? ((item is NSNull) ? "cellLoading" : "cell")
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier(indexPath: indexPath), for: indexPath)
        
        customCell(cell: cell, indexPath: indexPath, object: items[indexPath.row])
        
        return cell
    }
    
    func customCell(cell: UITableViewCell, indexPath: IndexPath, object: Any) {
        
    }
}

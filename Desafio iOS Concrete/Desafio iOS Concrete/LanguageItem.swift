//
//  LanguageItem.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 14/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import Foundation

struct LanguageItem {
    let title: String
    let language: String
}

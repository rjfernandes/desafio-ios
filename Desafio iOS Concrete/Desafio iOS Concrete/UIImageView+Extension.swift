//
//  UIImageView+Extension.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    func round() -> UIImageView {
        layer.cornerRadius = frame.height / 2
        clipsToBounds = true
        return self
    }
    
    func load(url: String?, default defaultImage: UIImage? = nil) {
        guard let url = url, let urlResource = URL(string: url) else {
            image = defaultImage
            return
        }
        kf.setImage(with: urlResource, placeholder: defaultImage)
    }
    
}

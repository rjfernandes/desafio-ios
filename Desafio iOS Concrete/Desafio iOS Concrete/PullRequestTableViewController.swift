//
//  PullRequestTableViewController.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import UIKit

class PullRequestTableViewController: BaseTableViewController {
    
    @IBOutlet weak var lblOpenCount: UILabel!
    @IBOutlet weak var lblCloseCount: UILabel!
    
    var repository: RepositoryModel!
    lazy var service = GithubService()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableHeaderView?.isHidden = true
        title = repository.name
        request()
    }
    
    func request() {
        items.removeAll()
        items += [ InfoRow() ]
        tableView.reloadData()
        service.pullRequests(repository: repository) { items, error in
            self.items.removeAll()
            if error != nil {
                self.items += [ InfoRow(error: error) ]
            }
            else {
                let opened = items.filter({ $0.isOpen }).count
                let closed = items.count - opened
                
                self.lblOpenCount.text = "\(opened) \(NSLocalizedString("opened", comment: ""))"
                self.lblCloseCount.text = "\(closed) \(NSLocalizedString("closed", comment: ""))"
                
                self.tableView.tableHeaderView?.isHidden = false
                
                self.items += items.sorted(by: { i1, i2 in i1.orderNo == i2.orderNo ? (i1.createDate ?? Date()).compare(i2.createDate ?? Date()) == .orderedDescending  : i1.orderNo < i2.orderNo } ) as [Any]
            }
            self.tableView.reloadData()
        }
    }
    
    override func customCell(cell: UITableViewCell, indexPath: IndexPath, object: Any) {
        if let cell = cell as? PullRequestCell, let item = object as? PullRequestModel {
            cell.pullRequest = item
        }
        else if let infoRow = object as? InfoRow  {
            if infoRow.isLoading {
                (cell.viewWithTag(100) as? UIActivityIndicatorView)?.startAnimating()
            }
            else if let cell = cell as? ErrorCell, let error = infoRow.error {
                cell.error = error
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let infoRow = items[indexPath.row] as? InfoRow, !infoRow.isLoading {
            request()
        }
        else if let pullRequest = items[indexPath.row] as? PullRequestModel, let url = URL(string: pullRequest.urlPage) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

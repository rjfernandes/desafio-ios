//
//  GithubService.swift
//  Desafio iOS Concrete
//
//  Created by Robson Fernandes on 13/07/17.
//  Copyright © 2017 Robson Fernandes. All rights reserved.
//

import Foundation
import ObjectMapper

class GithubService: RequestService {
    let baseGithubRepositoriesURL = "https://api.github.com/search/repositories?q=language:{language}&sort=stars&page={page}"
    
    var totalRecords = 0
    var recordsLoaded = 0
    var page = 1

    var languageItem = LanguageItem(title: "Java", language: "Java") {
        didSet {
            totalRecords = 0
            recordsLoaded = 0
            page = 1;
        }
    }
    
    var remainRecords: Int {
        return totalRecords - recordsLoaded
    }
    
    func changeLanguage(languageItem: LanguageItem) {
        self.languageItem = languageItem
    }
    
    /**
    Buscar os repositórios no Github
 
    - parameter page: Caso deseje forçar uma determinada página
    - parameter completion: Closure com hasMorePages:true/false e o model do github ou o erro retornado
    */
    func repositories(page forcedPage: Int? = nil, completion: @escaping (Bool, GithubModel?, ConcreteError?) -> Void) {
        
        if let forcedPage = forcedPage {
            page = forcedPage
        }
        
        let url = baseGithubRepositoriesURL
                        .replacingOccurrences(of: "{language}", with: languageItem.language)
                        .replacingOccurrences(of: "{page}", with: "\(page)")
        
        request(url: url) { (githubModel: GithubModel?, error) in
            guard let githubModel = githubModel else {
                completion(false, nil, error)
                return
            }
            
            self.page += 1
            
            if self.totalRecords == 0 {
                self.totalRecords = githubModel.total
            }
            
            self.recordsLoaded += githubModel.items.count
            
            completion(self.remainRecords > 0, githubModel, nil)
        }
    }
    
    /**
     Captura o nome completo do usuário
     
     - parameter user: UserModel do Usuário
     - parameter completion: Resposta da requisição com success: true/false - Caso true, a propriedade name contém o nome do usuário
     */
    func requestName(user: UserModel, completion: @escaping (Bool) -> Void) {
        requestDictionary(url: user.profileURL) { dict, error in
            user.name = dict?["name"] as? String
            completion( error == nil )
        }
    }
    /**
     Captura os pull requests
     
     - parameter repository: o model do repositório (RepositoryModel)
     - parameter completion: Closure com os pull requests ou o erro retornado
     */
    func pullRequests(repository: RepositoryModel, completion: @escaping ([PullRequestModel], ConcreteError?) -> Void) {
        request(url: repository.pullRequestURL, completion: completion)
    }
}

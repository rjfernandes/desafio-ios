//
//  Desafio_iOS_ConcreteTests.swift
//  Desafio iOS ConcreteTests
//
//  Created by Milena Maciel on 13/07/17.
//  Copyright © 2017 Milena Maciel. All rights reserved.
//

import XCTest
@testable import Desafio_iOS_Concrete

class Desafio_iOS_ConcreteTests: XCTestCase {

    lazy var service = GithubService()
    
    func testRepositories() {
        
        let language = LanguageItem(title: "Java", language: "Java")
        
        let expect  = expectation(description: "Execução correta da busca de resultados")
        
        service.changeLanguage(languageItem: language)
        service.repositories { (hasMorePages, githubModel, error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
            else if let githubModel = githubModel {
                print("Download realizado com sucesso! \(language.title) com \(githubModel.total) repositórios.")
                XCTAssert(true)
            }
            else {
                XCTFail("Erro não capturado")
            }
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 10) {
            if let error = $0 {
                XCTFail("Erro na espera da resposta do completion: \(error.localizedDescription)")
            }
        }
    }
    
    func testPullRequests() {
        let expect  = expectation(description: "Execução correta da busca de resultados")
        
        let dictionary = [
            "name": "RxJava",
            "pulls_url": "https://api.github.com/repos/ReactiveX/RxJava/pulls{/number}"
        ]
        
        guard let repository = RepositoryModel(JSON: dictionary) else {
            XCTFail("Erro na conversão do dictionary em model")
            return
        }
        
        service.pullRequests(repository: repository) { pullRequests, error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
            else {
                print("Download realizado com sucesso! \(repository.name) com \(pullRequests.count) pullRequests.")
                XCTAssert(true)
            }
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 10) {
            if let error = $0 {
                XCTFail("Erro na espera da resposta do completion: \(error.localizedDescription)")
            }
        }
        
    }
    
    func testRequestRealName() {
        let dictionary = [
            "login" : "rvadym",
            "url" : "https://api.github.com/users/rvadym"
        ]
        
        guard let user = UserModel(JSON: dictionary) else {
            XCTFail("Erro na conversão do dictionary em model")
            return
        }
        
        let expect  = expectation(description: "Execução correta da busca de resultados")
        
        service.requestName(user: user) { success in
            if let fullName = user.name, success {
                print("Nome completo: \(fullName)")
                XCTAssert(true)
            }
            else {
                XCTFail("Erro na requisição do nome")
            }
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 10) {
            if let error = $0 {
                XCTFail("Erro na espera da resposta do completion: \(error.localizedDescription)")
            }
        }
    }
}
